﻿using System;
using System.Collections.Generic;

namespace AStartAlgorithm
{
    public static class AStarAlgorithm
    {
        public static List<Node> GetShortestPath(Node[,] grid, Vector2Int startPos, Vector2Int targetPos)
        {

            var startNode = grid[startPos.x, startPos.y];
            var targetNode = grid[targetPos.x, targetPos.y];
            
            var openSet = new List<Node>();
            var closedSet = new HashSet<Node>();
            
            // to set the algorithm start point at startPos
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                // get Node with smallest fCost
                Node current = openSet[0];
                
                foreach (var node in openSet)
                {
                    // if node has smaller or equal fCost set it to current
                    if(current.FCost < node.FCost) continue;
                    if(current.HCost < node.HCost) current = node;
                }

                openSet.Remove(current);
                closedSet.Add(current);
                
                
                if(current == targetNode) break;

                foreach (var neighbour in GetNeighbours(grid, current.Position))
                {
                    // neighbour already checked or not walkable
                    if(!neighbour.Walkable || closedSet.Contains(neighbour)) continue;
                    var newGCost = current.GCost + GetNodeDistance(current, neighbour);

                    if (!openSet.Contains(neighbour) || newGCost < neighbour.GCost) // check if shorter path
                    {
                        // set neighbour Costs
                        neighbour.GCost = newGCost;
                        neighbour.HCost = GetNodeDistance(neighbour, targetNode);

                        neighbour.Parent = current;
                        
                        if(!openSet.Contains(neighbour)) openSet.Add(neighbour);
                    }
                    
                }
            }

            if (targetNode.Parent is null) throw new Exception("No way to get there! No path found");

            return RetracePath(targetNode);
        }

        private static List<Node> RetracePath( Node endNode)
        {
            var path = new List<Node>();
            var current = endNode;

            while (current != null)
            {
                path.Add(current);
                current = current.Parent;
            }

            path.Reverse();
            
            return path;
        }

        private static int GetNodeDistance(Node a, Node b)
        {
            var dist = Vector2Int.Distance(a.Position, b.Position);
            var shorterDist = Math.Min(dist.x, dist.y);
            return shorterDist * 14 + shorterDist == dist.x ? dist.y : dist.x * 10;
        }

        private static List<Node> GetNeighbours(Node[,] grid, Vector2Int nodePosition)
        {
            var neighbours = new List<Node>();

            for (int x = nodePosition.x - 1; x <= nodePosition.x + 1; x++)
            {
                for (int y = nodePosition.y - 1; y <= nodePosition.y + 1; y++)
                {
                    try
                    {
                        neighbours.Add(grid[x, y]);
                    }
                    catch {/* ignored */}
                }
            }

            neighbours.Remove(grid[nodePosition.x, nodePosition.y]);
            return neighbours;
        }

        public static Node[,] GenerateEmptyGrid(Vector2Int size)
        {
            var grid = new Node[size.x, size.y];
            
            for (int x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    grid[x, y] = new Node(new Vector2Int(x, y), true);
                }
            }

            return grid;
        }

        public static Vector2Int GetRandomWalkablePos(Node[,] grid)
        {
            var rand = new Random();
            
            Vector2Int pos = null;
            while (pos is null)
            {
                var randPos = new Vector2Int(rand.Next(grid.GetLength(0)), rand.Next(grid.GetLength(1)));
                if (grid[randPos.x, randPos.y].Walkable) pos = randPos;
            }

            return pos;
        }
    }
}