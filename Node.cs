﻿namespace AStartAlgorithm
{
    public class Node
    {
        public readonly Vector2Int Position;
        public bool Walkable;
        public Node Parent;
        public int GCost, HCost;
        public int FCost => GCost + HCost;

        public Node(Vector2Int position,bool walkable)
        {
            Position = position;
            Walkable = walkable;
        }
    }
}