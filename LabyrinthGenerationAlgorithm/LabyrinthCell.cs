﻿using System.Collections.Generic;
using AStartAlgorithm;

namespace LabyrinthGenerationAlgorithm
{
    public class LabyrinthCell
    {
        public LabyrinthCell Previous;
        public readonly Vector2Int Position;

        public Dictionary<Vector2Int, Node> NodePositions = new Dictionary<Vector2Int, Node>();
        public readonly Node[,] Nodes = new Node[3, 3];

        public LabyrinthCell(Vector2Int position)
        {
            Position = position;
            
            // in one Labyrinth cell are 3 vertical Nodes so Node.Position = LabyrinthCell.pos.x * 3
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    Nodes[x, y] = new Node(new Vector2Int(position.x * 3 + x, position.y * 3 + y), false);
                }
            }
            
            // set middle node to walkable
            Nodes[1, 1].Walkable = true;

        }
        
    }
}