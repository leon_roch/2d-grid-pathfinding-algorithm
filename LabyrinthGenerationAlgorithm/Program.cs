﻿using System;
using System.Text;
using AStartAlgorithm;

namespace LabyrinthGenerationAlgorithm
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var rand = new Random();
            
            // Draw Labyrinth
            var labyrinthGrid = LabyrinthGenerator.GenerateLabyrinth(new Vector2Int(rand.Next(4, 24), rand.Next(4, 10)), false);
            
            // Draw Labyrinth
            // LabyrinthGenerator.LogGrid(labyrinthGrid, null);
            
            // Convert Grid to A* Algorithm compatible Grid
            var grid = LabyrinthGenerator.LabyrinthToNodeGrid(labyrinthGrid);

            // Get path
            var path = AStarAlgorithm.GetShortestPath(grid, new Vector2Int(1, 1), 
                new Vector2Int(grid.GetLength(0) -2, grid.GetLength(1) -2));
            
            // Draw all
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                var str = new StringBuilder();
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    
                    switch (path.Contains(grid[x,y]))
                    {
                        case true: str.Append("o ");
                            break;
                        case false: 
                            str.Append(grid[x, y].Walkable ? "  " : "■ ");
                            break;
                    }
                }
                Console.WriteLine(str);
            }
        }
    }
}