﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using AStartAlgorithm;

namespace LabyrinthGenerationAlgorithm
{
    public class LabyrinthGenerator
    {
        public static LabyrinthCell[,] GenerateLabyrinth(Vector2Int size, bool debug)
        {
            var grid = new LabyrinthCell[size.x,size.y];

            for (int x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    grid[x, y] = new LabyrinthCell(new Vector2Int(x, y));
                }
            }
            
            /*
            1. Pick any random cell in the grid (In this implementation we use the upper left hand corner.)
            2. Find a random neighboring cell that hasn't been visited yet.
            3. If you find one, strip the wall between the current cell and the neighboring cell.
            4. If you don't find one,  return to the previous cell.
            5. Repeat steps 2 and 3  (or steps 2 and 4)  for every cell in the grid.
            */

            var visitedCells = new HashSet<LabyrinthCell>();
            var rand = new Random();

            var start = grid[rand.Next(grid.GetLength(0)), rand.Next(grid.GetLength(1))];
            var current = start;

            while (visitedCells.Count != grid.Length)
            {
                if (debug)
                {
                    Console.WriteLine("\n\n");
                    LogGrid(grid, current);
                    Thread.Sleep(100);
                }
                
                
                visitedCells.Add(current);
                
                if (GetRandomNeighbour(grid, current, out var neighbour, rand, visitedCells))
                {
                    neighbour.Previous = current;
                    StripWallBetween(current, neighbour);
                    current = neighbour;
                    continue;
                }
                
                // go to previous cell because here everything is visited
                current = current.Previous;
            }
            
            return grid;
        }
        
        private static void StripWallBetween(LabyrinthCell a, LabyrinthCell b)
        {
            // dist could be (0,1) (0,-1) (1,0) (-1,0)
            var dist = Vector2Int.Direction(a.Position, b.Position);

            if (dist.ToString() == Vector2Int.Up.ToString())
            {
                a.Nodes[1, 0].Walkable = true;
                b.Nodes[1, 2].Walkable = true;
            }
            else if (dist.ToString() == Vector2Int.Down.ToString())
            {
                a.Nodes[1, 2].Walkable = true;
                b.Nodes[1, 0].Walkable = true;
            }
            else if (dist.ToString() ==  Vector2Int.Right.ToString())
            {
                a.Nodes[2, 1].Walkable = true;
                b.Nodes[0, 1].Walkable = true;
            }
            else if (dist.ToString() == Vector2Int.Left.ToString())
            {
                a.Nodes[0, 1].Walkable = true;
                b.Nodes[2, 1].Walkable = true;
            }
        }

        private static bool GetRandomNeighbour(LabyrinthCell[,] grid, LabyrinthCell currentCell, out LabyrinthCell neighbour, Random rand, HashSet<LabyrinthCell> visited)
        {
            var currentPos = currentCell.Position;
            var neighbours = new List<LabyrinthCell>();
            
            // get the neighbours with the directions (0,1) (0,-1) (1,0) (-1,0)
            var possible = new List<int>() {-1, 1, 0, 0};
            
            for (int i = 0; i < possible.Count; i++)
            {
                try
                {
                    var x = currentPos.x + possible[i];
                    // 'possible[possible.Count - 1 - i]' is reverse of possible[i] because I want only these four directions
                    var y = currentPos.y + possible[possible.Count - 1 - i];
                    if(!visited.Contains(grid[x,y])) neighbours.Add(grid[x, y]);
                }
                catch {/* ignored */}
            }
            
            if (neighbours.Count == 0)
            {
                neighbour = null;
                return false;
            }
            neighbour = neighbours[rand.Next(neighbours.Count)];
            return true;
        }

        public static void LogGrid(LabyrinthCell[,] grid, LabyrinthCell current)
        {
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                for (int yy = 0; yy < 3; yy++)
                {
                    var str = new StringBuilder();

                    for (int x = 0; x < grid.GetLength(0); x++)
                    {
                        var nodes = grid[x, y].Nodes;

                        for (int xx = 0; xx < nodes.GetLength(0); xx++)
                        {
                            
                            if (grid[x, y] == current)
                            {
                                str.Append(nodes[xx, yy].Walkable ? "  " : "# ");
                                continue;
                            }
                            str.Append(nodes[xx,yy].Walkable ? "  " : "■ ");
                        }
                    }
                    
                    Console.WriteLine(str);
                }
            }
        }

        public static Node[,] LabyrinthToNodeGrid(LabyrinthCell[,] labyrinthGrid)
        {
            var nodeGrid = new Node[labyrinthGrid.GetLength(0) * 3,labyrinthGrid.GetLength(1) * 3];

            foreach (var labyrinthCell in labyrinthGrid)
            {
                foreach (var node in labyrinthCell.Nodes)
                {
                    var nodePos = node.Position;
                    nodeGrid[nodePos.x, nodePos.y] = node;
                }
            }
            
            return nodeGrid;
        }
    }
}