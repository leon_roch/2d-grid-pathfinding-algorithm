﻿using System;

namespace AStartAlgorithm
{
    public class Vector2Int
    {
        public int x, y;
        public static Vector2Int Zero = new Vector2Int(0, 0);
        public static Vector2Int Up = new Vector2Int(0, 1);
        public static Vector2Int Down = new Vector2Int(0, -1);
        public static Vector2Int Right = new Vector2Int(1, 0);
        public static Vector2Int Left = new Vector2Int(-1, 0);

        public Vector2Int(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2Int Distance(Vector2Int a, Vector2Int b)
        {
            var distX = Math.Abs(a.x - b.x);
            var distY = Math.Abs(a.y - b.y);
            return new Vector2Int(distX, distY);
        }
        
        public static Vector2Int Direction(Vector2Int a, Vector2Int b)
        {
            var distX = Math.Sign(b.x - a.x);
            var distY = Math.Sign(a.y - b.y);
            return new Vector2Int(distX, distY);
        }

        public override string ToString()
        {
            return $"({this.x}, {this.y})";
        }
    }
}