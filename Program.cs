﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AStartAlgorithm
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var gridSize = 5;
            var grid = AStarAlgorithm.GenerateEmptyGrid(new Vector2Int(gridSize, gridSize));
            var path = AStarAlgorithm.GetShortestPath(grid, Vector2Int.Zero, 
                AStarAlgorithm.GetRandomWalkablePos(grid));
            
            for (int x = 0; x < gridSize; x++)
            {
                var str = new StringBuilder();
                for (int y = 0; y < gridSize; y++)
                {
                    switch (path.Contains(grid[x,y]))
                    {
                        case true: str.Append("$ ");
                            break;
                        case false: 
                        str.Append(grid[x, y].Walkable ? ". " : "o ");
                            break;
                    }
                }
                Console.WriteLine(str);
            }
        }
    }
}